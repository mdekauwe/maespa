
A soil-plant-atmosphere model based on MAESTRA and SPA
===================================================
  
Compiles with:
* Intel Visual Fortran Compiler (version >10). 
* gfortran

A Makefile is provided to compile Maes* on a Mac (thanks to Martin de Kauwe and Alejandro Morales).

The dependency on IFPORT has been recently removed. The same code now compiles with both compilers.
